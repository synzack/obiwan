.. obiwan documentation master file, created by
   sphinx-quickstart on Tue Oct  2 13:41:20 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to obiwan's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 4

   Core <src.core>
   Modules <src.modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

