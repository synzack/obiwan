src Package
===========

:mod:`bot` Module
-----------------

.. automodule:: src.bot
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    src.core
    src.modules

