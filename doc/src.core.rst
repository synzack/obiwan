core Package
============

:mod:`bot` Module
-----------------

.. automodule:: src.core.bot
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`chan` Module
------------------

.. automodule:: src.core.chan
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`config` Module
--------------------

.. automodule:: src.core.config
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`cron` Module
------------------

.. automodule:: src.core.cron
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`data` Module
------------------

.. automodule:: src.core.data
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`modules` Module
---------------------

.. automodule:: src.core.modules
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`network` Module
---------------------

.. automodule:: src.core.network
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`privmsg` Module
---------------------

.. automodule:: src.core.privmsg
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`protocol` Module
----------------------

.. automodule:: src.core.protocol
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`user` Module
------------------

.. automodule:: src.core.user
    :members:
    :undoc-members:
    :show-inheritance:

