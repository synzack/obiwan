modules Package
===============

:mod:`autoreply_example` Module
-------------------------------

.. automodule:: src.modules.autoreply_example
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`chans` Module
-------------------

.. automodule:: src.modules.chans
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`help` Module
------------------

.. automodule:: src.modules.help
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`links` Module
-------------------

.. automodule:: src.modules.links
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`pat` Module
-----------------

.. automodule:: src.modules.pat
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`poke` Module
------------------

.. automodule:: src.modules.poke
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`quote` Module
-------------------

.. automodule:: src.modules.quote
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`remote` Module
--------------------

.. automodule:: src.modules.remote
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`say` Module
-----------------

.. automodule:: src.modules.say
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`stalk` Module
-------------------

.. automodule:: src.modules.stalk
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`thegame` Module
---------------------

.. automodule:: src.modules.thegame
    :members:
    :undoc-members:
    :show-inheritance:

