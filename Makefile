
PYTHONFILES=$(wildcard src/*.py src/core/*.py src/modules/*.py)

all: doc

.PHONY: doc
doc: $(PYTHONFILES)
	sphinx-apidoc -o doc -f -H obiwan -A"Fireteam" src/ 
	make -C doc html
	rm -rf /home/fire/public_html/bot
	cp -Rf doc/_build/html /home/fire/public_html/bot

clean:
	@make -C doc clean
	@find -name "*.pyc" -exec rm -v {} +

regen-doc:
	sphinx-apidoc -o doc -F -H obiwan -A"Fireteam" src/ 
	# Needs changes in conf.py and index
