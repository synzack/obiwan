#!/usr/bin/python3
# -*- coding: utf-8 -*-

from .network import ThreadedWorker
from datetime import datetime, timedelta

class AnyMatch(set):
    """ A set that matches everything """
    def __contains__(self, item):
        return True
any_match = AnyMatch()

class TimedEvent(object):
    """
    Usage examples:

    >>> halt = Event()
    >>> crontab = Gron(halt)
    >>> crontab.start()
    # run **my_fun()** every two hours
    >>> crontab.add_job(1, TimedEvent(my_fun, minutes=[0, 15, 30, 45]))
    # run **my_other_fun()** every two hours
    >>> crontab.add_job(2, TimedEvent(my_other_fun, hours=range(0, 24, 2)))
    # run **clean_files(file_list)** every two month, on 1st
    >>> crontab.add_job(3, TimedEvent(clean_files, minutes=0, hours=0, days=1,
                                   month=range(0, 12, 2), [file_list]))
    # remove job 1 (warning: any type can be used to index jobs)
    >>> crontab.del_job(1)
    # Stop Gron
    >>> halt.Set()
    """
    def __init__(self, fun=None,
            seconds=0,
            minutes=any_match,
            hours=any_match,
            days=any_match,
            months=any_match,
            dow=any_match,
            args=(),
            kwargs={},
            debug=False):
        self.seconds = to_set(seconds)
        self.minutes = to_set(minutes)
        self.hours = to_set(hours)
        self.days = to_set(days)
        self.months = to_set(months)
        self.dow = to_set(dow)
        self.fun = fun
        self.args = args
        self.kwargs = kwargs
        self.debug = debug
        self._last_exec = None

    def _check_last_exec(self, time):
        """ Returns True if the function has not aleady been executed """
        if not self._last_exec:
            return True
        return (time - self._last_exec) >= timedelta(seconds=1)

    def match(self, t):
        assert isinstance(t, datetime)
        return ((t.second in self.seconds) and
                (t.minute in self.minutes) and
                (t.hour in self.hours) and
                (t.day in self.days) and
                (t.month in self.months) and
                (t.weekday() in self.dow))

    def set_fun(self, fun, *args, **kwargs):
        self.fun = fun
        self.args = args
        self.kwargs = kwargs

    def check(self, time):
        assert self.fun is not None
        if self.debug:
            print(time.second, "-", self.seconds, time.second in self.seconds)
            print(time.minute, "-", self.minutes, time.minute in self.minutes)
            print(time.hour, "-", self.hours)
            print(time.day, "-", self.days)
            print(time.month, "-", self.months)

        if self._check_last_exec(time) and self.match(time):
            self._last_exec = time
            self.fun(*self.args, **self.kwargs)


def to_set(thing):
    if isinstance(thing, AnyMatch):
        return thing
    if isinstance(thing, int):
        return set([thing])
    else:
        return set(thing)
