#!/usr/bin/python3
# -*- coding: utf-8 -*-


class User(object):
    def __init__(self, nick, username="", hostname="", realname=""):
        self.nick = nick
        self.username = username
        self.realname = realname
        self.hostname = hostname

    def __repr__(self):
        if self.hostname:
            if self.username:
                return "%s!%s@%s" % (self.nick, self.username, self.hostname)
            else:
                return "%s@%s" % (self.nick, self.hostname)
        else:
            if self.username:
                return "%s(%s)" % (self.nick, self.username)
            else:
                return self.nick

    def __str__(self):
        return repr(self)

    def __eq__(self, user):
        if not isinstance(user, User):
            return False
        return user.nick == self.nick

    def __ne__(self, user):
        return not self.__eq__(user)

    def __hash__(self):
        return hash(self.nick)
