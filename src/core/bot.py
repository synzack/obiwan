#!/usr/bin/python3
# -*- coding: utf-8 -*-

from .network import IRCConnection, ThreadedWorker
from .protocol import IRCMessage
from .modules import ModuleLoader
from .chan import Chan
from .privmsg import Privmsg
from .config import Config, info, debug, error

class Bot(IRCConnection):
    def __init__(self):
        if not Config.has_instance():
            raise Exception("Config uninitialized")
        config = Config.instance()
        self._server = serv = config["servers"][0]
        IRCConnection.__init__(self, serv["host"], serv["port"])
        self.module_loader = ModuleLoader(self, config.modules)
        config.add_hook(self.module_loader) # ajout d'un hook de reload
        self.scheduler = ThreadedWorker(self.module_loader.exec_timed_functions,
                self.stop, .5)
        self.scheduler.start()
        self.nick = serv["nick"]
        self.chans = {}

    def __del__(self):
        debug("Delete bot")
        del self.module_loader
        del self.scheduler

    def start(self):
        """ Send id and join chans """
        IRCConnection.start(self)
        self.nick_cmd(self._server["nick"])
        # TODO: add failover nick
        self.user_cmd(self._server["user"], self._server["hostname"],
                self._server["realname"])
        for chan in self._server["chans"]:
            self.join_cmd(chan["name"], chan.get("password", None))
            if "hello_msg" in chan:
                self.privmsg_cmd(chan["name"], chan["hello_msg"])
            self.chans[chan["name"]] = Chan(chan["name"])

    def recv(self, message):
        """ Handle a received message by using the appropriate modules """
        def call_hooks(name, message):
            self.module_loader.call_hooks(name, message)

        IRCConnection.recv(self, message)

        if message.command == "PRIVMSG":
            # special cases for privmsg<F5>
            privmsg = Privmsg(message)
            if privmsg.on_chan:
                call_hooks("PRIVMSG_CHAN", privmsg)
            else:
                call_hooks("PRIVMSG_QUERY", privmsg)
        else:
            call_hooks(message.command, message)


