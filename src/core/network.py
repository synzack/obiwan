#!/urs/bin/python3
# -*- coding: utf-8 -*-

import socket
from threading import Thread, Event
from time import sleep
from .protocol import IRCMessage
from .config import debug

class IRCNetworkError(Exception):
    pass


class NetworkMessage(object):
    """
    A simple queue used internally by IRCConnection containing the messages to
    send
    """
    msgs = []
    def __init__(self):
        Event.__init__(self)

    def set(self, msg):
        """
        Add message to queue
        """
        self.msgs.append(msg)

    def get(self):
        """
        Get first message in queue or return None
        """
        try:
            return self.msgs.pop(0)
        except IndexError:
            return None

    def is_set(self):
        return self.msgs != []


class ThreadedWorker(Thread):
    """
    Execute a function at regular interval
    """
    def __init__(self, fun, halt, interval=0.5):
        """
        `halt' is a threading.Event
        """
        self.fun = fun
        self.halt = halt
        Thread.__init__(self)
        self.interval = interval

    def run(self):
        fun = self.fun
        interval = self.interval
        while not self.halt.is_set():
            fun()
            sleep(interval)


class IRCBaseConnection(object):
    """
    Create and manage an IRC connection.
    Two threaded workers are used internally to send and
    receive messages.
    """
    def _socket_wrap(fun):
        def new_fun(*args, **kwargs):
            try:
                return fun(*args, **kwargs)
            except OSError as err:
                raise IRCNetworkError("In function %s: %s" % (fun.__name__, err))
        return new_fun

    def __init__(self, host, port):
        self.host, self.port = host, port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                socket.IPPROTO_TCP)
        self.stop = Event()
        #self.s.settimeout(5)
        self.t_recv = ThreadedWorker(self._recv, self.stop)
        self.t_recv.daemon = True
        self.t_send = ThreadedWorker(self._send, self.stop)
        self.t_send.daemon = True

        self.message = NetworkMessage()
        self._connected = False

    @_socket_wrap
    def start(self):
        """
        Connect to the server and start the send/recv threads
        """
        self.s.connect((self.host, self.port))
        self.t_recv.start()
        self.t_send.start()
        self._connected = True

    @_socket_wrap
    def _send(self):
        if not self._connected:
            return
        if self.message.is_set():
            self.s.send(self.message.get().encode("utf-8", "replace"))

    @_socket_wrap
    def _recv(self):
        def socket_receive(socket):
            try:
                return str(socket.recv(1024), "utf-8", "replace")
            except OSError as err:
                return ""


        if not self._connected:
            return

        prev = ""
        msg = socket_receive(self.s)
        while msg:
            messages = msg.split("\r\n")
            if prev:
                messages[0] = prev + messages[0]
            for m in messages[:-1]:
                self.recv(IRCMessage(m))
            prev = messages[-1] if messages[-1] else ""
            if prev:
                msg = socket_receive(self.s)
            else:
                msg = ""

    def send(self, message):
        """
        Send an IRC message. Use protocol.IRCMessage to build it,
        or use a higher level method in IRCConnection.
        """
        if isinstance(message, IRCMessage):
            m = message.message
        else:
            m = message
        assert(isinstance(m, str))
        self.message.set(m)

    def recv(self, message):
        """
        Called when a message is received.
        This method should be overwritten to handle more commands
        """
        if message.command == "PING":
            self.send(IRCMessage((None, "PONG", [message.params[0]])))
        else:
            debug(message)

    @_socket_wrap
    def close(self):
        """
        Close the connection
        """
        self.stop.set()
        self._connected = False
        self.s.shutdown(socket.SHUT_RDWR)
        self.s.close()


class IRCConnection(IRCBaseConnection):
    """
    Adds a few high level methods to IRCBaseConnection
    """
    def __init__(self, host, port):
        IRCBaseConnection.__init__(self, host, port)

    def nick_cmd(self, nickname):
        """
        Send a NICK command
        """
        self.send(IRCMessage( (None, "NICK", [nickname]) ))

    def user_cmd(self, username, hostname, realname):
        """
        Send a USER command
        """
        self.send(IRCMessage( (None, "USER", [username, hostname, hostname,
            realname])) )

    def join_cmd(self, chan, password=None):
        """
        Send a JOIN command
        """
        if isinstance(chan, (list, tuple)):
            chan = ",".join(chan)
        assert(chan.find(" ") == -1)
        self.send(IRCMessage( (None, "JOIN", [chan, password] if password
            else [chan]) ))

    def privmsg_cmd(self, dest, msg):
        """
        Send a PRIVMSG command
        """
        if isinstance(dest, (list, tuple)):
            dest = ",".join(dest)
        assert(dest.find(" ") == -1)
        self.send(IRCMessage( (None, "PRIVMSG", [dest, msg]) ))

    def whois_cmd(self, user):
        """
        Send a WHOIS command
        """
        if isinstance(user, (list, tuple)):
            user = ",".join(user)
        assert(user.find(" ") == -1)
        self.send(IRCMessage( (None, "WHOIS", [user]) ))

if __name__ == "__main__":
    conn = IRCConnection("irc.rezosup.org", 6667)
    conn.start()
    conn.send(IRCMessage((None, "NICK", ["obiwankenobi"])))
    conn.send(IRCMessage((None, "USER", ["botounet", "demic.eu",
        "demic.eu", "botounet v2"])))
    conn.send(IRCMessage((None, "JOIN", ["#firebot"])))
    #conn.send(IRCMessage((None, "PRIVMSG", ["#firebot", "Salut les moches"])))
    try:
        while True:
            sleep(1)
    except KeyboardInterrupt:
        conn.close()

