#!/usr/bin/python3
# -*- coding: utf-8 -*-

from importlib import import_module
from functools import wraps
import inspect
import threading
from collections import defaultdict
from datetime import datetime

from .privmsg import Privmsg
from .cron import TimedEvent
from .data import DataHandler
from .config import Config, ConfigHook

"""
Contains some utilities that can be useful to write modules

    Bot instance 
         -> ModuleLoader - modules |- DynamicModule
                                   |- DynamicModule
                                   |- DynamicModule
                         - hooks   hook_name -> method1, method2, ...
                                   hook_name -> method3, method4, ...

"""


class ModuleError(Exception):
    pass


class BaseModule(object):
    """
    The base class for any module, used by the core engine to register hooks
    """
    _module_name = None

    def __init__(self, bot, with_datas=False):
        self._bot = bot
        self.with_datas = with_datas
        self.datahandler = None

    def __hash__(self):
        # Génère un hash spécifique à chaque version de la classe
        return hash(inspect.getsource(self.__class__))

    @classmethod
    def command_hook(cls, command, help_="", command_char="!"):
        """
        A decorator aimed to handle commands
        If a privmsg contains the right commands, it executes the function, and the
        command is removed from the message.
        """
        def wrapper(fun):
            if (not fun.__name__.startswith("on_PRIVMSG_CHAN") and
                not fun.__name__.startswith("on_PRIVMSG_QUERY")):
                raise ModuleError("You can only put command hooks on "
                                  "PRIVMSG_CHAN and QUERY!")
            @wraps(fun)
            def new_fun(self, privmsg):
                assert(isinstance(privmsg, Privmsg))
                cmd = command_char + command
                if privmsg.msg == cmd or privmsg.msg.startswith(cmd + " "):
                    # remove the command and execute the function
                    privmsg._msg = privmsg._msg.replace(cmd, "").lstrip()
                    privmsg._params = [privmsg._params[0], privmsg._msg]
                    return fun(self, privmsg)

            if help_:
                new_fun.help = (command, help_)
                #module_name = get_module(fun)

            return new_fun
        return wrapper

    @classmethod
    def timed_function(cls, event, args=(), kwargs={}):
        """
        Execute a function at regular interval
        """
        assert (isinstance(event, TimedEvent))
        def wrapper(fun):
            fun.event = event
            fun.args = args
            fun.kwargs = kwargs
            fun.last_exec = None
            module_name = get_module(fun)
            return fun
        return wrapper

    @classmethod
    def savedatas(cls, fun):
        """
        A decorator that saves the datas each time the function is called
        """
        @wraps(fun)
        def new_fun(self, *args, **kwargs):
            result = fun(self, *args, **kwargs)
            if self.datahandler:
                self.datahandler.save()
            return result
        return new_fun

    @property
    def module_name(self):
        if not self._module_name:
            self._module_name = get_module(self)
        return self._module_name

    @property
    def bot(self):
        return self._bot

    @property
    def datas(self):
        """
        Get the module datas
        """
        if not self.datahandler:
            return None
        return self.datahandler.datas

    def error(self, string):
        """
        Log an error message
        """
        Config.instance().module_logger.error("[%s] %s" % (self.module_name, string))

    def info(self, string):
        """
        Log an info message
        """
        Config.instance().module_logger.info("[%s] %s" % (self.module_name, string))

    def debug(self, string):
        """
        Log a debug message
        """
        Config.instance().module_logger.debug("[%s] %s" % (self.module_name, string))

    def set_datahandler(self, datahandler):
        assert(isinstance(datahandler, DataHandler))
        self.datahandler = datahandler


class PrefixDict(dict):
    # A hash map with possible multiple values, and search by prefix
    def __contains__(self, item):
        for elem in self:
            if elem.startswith(item):
                return True
        return False

    def __getitem__(self, item):
        values = []
        for elem in self:
            if elem.startswith(item):
                values.extend(dict.__getitem__(self, elem))
        if not values:
            raise KeyError
        return values

    def __setitem__(self, item, value):
        if item in self.keys():
            dict.__getitem__(self, item).append(value)
        else:
            dict.__setitem__(self, item, [value])


class DynamicModule(object):
    """
    Contains infos on a module
    """
    # static attr, set by a loader
    bot = None
    def __init__(self, module, classes):
        assert(self.bot != None)
        for _, cls in classes:
            assert issubclass(cls, BaseModule) and cls != BaseModule
        self._module = module
        self._classes = dict((class_name, (None, cls, None)) for class_name, cls in classes)
        self._instances = {}
        self._hooks = PrefixDict()
        self._timed_events = []
        self._helps_pub = {}
        self._helps_priv = {}
        self._load_instances()
        self._load_hooks()

    def __del__(self):
        for (_, _, instance) in self._classes.values():
            del instance

    def __eq__(self, item):
        """
        Check each hash for each class and returns false if something changed.
        """
        if not isinstance(item, DynamicModule):
            return False
        for (hsh, cls, _) in item.classes:
            if not cls.__name__ in self._classes:
                return False
            if self._classes[cls.__name__][0] != hsh:
                return False
        return True

    def __ne__(self, item):
        return not self.__eq__(item)

    def _load_instances(self):
        for clsname, (_, cls, _) in self._classes.items():
            instance = cls(self.bot)
            self._classes[clsname] = (hash(instance), cls, instance)
            if instance.with_datas:
                instance.set_datahandler(DataHandler(get_module(instance)))

    def _load_timed_jobs(self):
        """
        Store all the methods decorated with 'timed_function'
        """
        for (_, _, instance) in self._classes.values():
            for method_name in dir(instance):
                method = getattr(instance, method_name)

    def _load_hooks(self):
        for (_, _, instance) in self._classes.values():
            for method_name in dir(instance):
                method = getattr(instance, method_name)
                # Store events
                if hasattr(method, "event"):
                    event = method.event
                    event.set_fun(method, *method.args, **method.kwargs)
                    self._timed_events.append(event)

                if not method_name.startswith("on_"):
                    continue

                # Store hooks
                hook_name = method_name.replace("on_", "")
                self._hooks[hook_name] = method # does not override an existing value

                # Store commands help
                if hasattr(method, "help"):
                    cmd, help_text = method.help
                    if hook_name.startswith("PRIVMSG_CHAN"):
                        self._helps_pub[cmd] = help_text
                    elif hook_name.startswith("PRIVMSG_QUERY"):
                        self._helps_priv[cmd] = help_text

    def call_hooks(self, hook_name, irc_message):
        """
        Method used to call all the matching hooks with the irc_message as
        argument
        """
        if hook_name in self._hooks:
            methods = self._hooks[hook_name]
            for method in methods:
                method(irc_message)

    def check_timed_functions(self, time):
        """
        Check for every function if it should be executed
        (ie. if the time matches)
        """
        for event in self._timed_events:
            threading.Thread(target=event.check, args=(time,)).start()

    @property
    def module(self):
        """ Get the python module associated """
        return self._module

    @property
    def classes(self):
        """ Get a list of (hash, class, instance) """
        return self._classes.values()

    def help(self, priv=False):
        return self._helps_priv if priv else self._helps_pub

    @property
    def hooks(self):
        """ Get a dict containing each hook and the methods associated """
        return self._hooks

    @property
    def name(self):
        """ Get the module name """
        return self._module.__name__


class ModuleLoader(ConfigHook):
    def __init__(self, bot, module_names):
        self._bot = bot
        DynamicModule.bot = bot
        self._modules = {}
        self._lock = threading.RLock()
        self.load_all_modules(module_names)

    def __del__(self):
        for module_name, module in self._modules.items():
            print("Delete module %s" % module_name)
            del module

    def _fetch_python_module(self, module_name):
        """
        Import data and returns a DynamicModule instance
        """
        # TODO: handle errors
        module = import_module("..modules.%s" % module_name, __package__)
        classes = inspect.getmembers(module, lambda e: inspect.isclass(e) and
                issubclass(e, BaseModule) and e != BaseModule)
        return DynamicModule(module, classes)

    def _locked(fun):
        def new_fun(self, *args, **kwargs):
            with self._lock:
                return fun(self, *args, **kwargs)
        return new_fun

    def load_module(self, name):
        """ Load or reload a module """
        module = self._fetch_python_module(name)
        if name in self._modules:
            # check if the module code has been modified
            if module != self._modules[name]:
                print("Reloading module '%s'" % name)
                self.unload_module(name)
            else:
                return

        self._modules[name] = module

    def unload_module(self, name):
        """ Remove a module and its hooks """
        if not name in self._modules:
            return
        del self._modules[name]

    @_locked
    def load_all_modules(self, module_names):
        """ Load all the module and all the hooks """
        self._modules = {}
        for module_name in module_names:
            self.load_module(module_name)

    def call_hooks(self, hook_name, irc_message):
        for module in self._modules.values():
            module.call_hooks(hook_name, irc_message)

    @property
    def modules(self):
        """ Get a list of DynamicModules """
        return self._modules.values()

    def help(self, priv=False):
        """ Get a dict of { command -> help } """
        help_commands = {}
        for module in self._modules.values():
            help_commands.update(module.help(priv))
        return help_commands

    @_locked
    def exec_timed_functions(self):
        for module in self._modules.values():
            module.check_timed_functions(datetime.now())

    # Config hook:
    @_locked
    def on_reload(self, old_config, new_config):
        old_modules = set(old_config["modules"])
        new_modules = set(new_config["modules"])
        to_remove = old_modules - new_modules
        for module in to_remove:
            self.unload_module(module)
        for module in new_modules:
            self.load_module(module)


def get_module(obj):
    return inspect.getmodule(obj).__name__

