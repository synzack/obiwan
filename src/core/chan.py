#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Chan(object):
    def __init__(self, name):
        self.name = name
        self.users = set()

    def add_users(self, users):
        assert(isinstance(users, (list, set)))
        self.users.update(users)

    def __repr__(self):
        return "<Chan %s (%d users)>" % (self.name, len(self.users))

    def __str__(self):
        return repr(self)
