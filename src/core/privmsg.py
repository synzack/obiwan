#!/usr/bin/python3
# -*- coding: utf-8 -*-


from .protocol import IRCMessage

class Privmsg(IRCMessage):
    def __init__(self, message):
        IRCMessage.__init__(self, message)
        self._exp = self.user.nick
        self._dest = self._params[0]
        self._msg = self._params[1]

    def __repr__(self):
        return "<Privmsg from=%s to=%s \"%s\">" % (
                self._exp, self._dest, self._msg)

    def __str__(self):
        return self.__repr__()

    @property
    def on_chan(self):
        """ False if the message is a query """
        return self._dest.startswith("#")

    @property
    def msg(self):
        return self._msg

    @property
    def exp(self):
        return self._exp

    @property
    def dest(self):
        return self._dest

    def build_reply(self, message):
        """
        Create an IRCMessage that can be sent back directly to the sender, by
        using a string 'message' and the sender infos.
        """
        if self.on_chan:
            return IRCMessage((None, "PRIVMSG", [self.dest, "%s: %s" %
                (self.exp, message)]))
        else:
            return IRCMessage((None, "PRIVMSG", [self.exp, message]))
        

