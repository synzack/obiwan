import pickle
import os
from .config import Config, ConfigError


class ModuleDatas(dict):
    pass


class DataHandler(object):
    """
    module specific datahandler
    """
    def __init__(self, module_name):
        self._module_name = module_name
        self._datas = self._load_data()

    def _get_datadir(self):
        try:
            return Config.instance()["global"]["data_directory"]
        except ConfigError:
            # TODO: warning
            return None

    def _load_data(self):
        module_name = self._module_name
        datadir = self._get_datadir()
        if datadir:
            files = os.listdir(datadir)
            if module_name in files:
                with open(os.path.join(datadir, module_name), "rb") as file:
                    try:
                        return pickle.load(file)
                    except pickle.PickleError:
                        pass
        return ModuleDatas()

    def _save_datas(self, datas):
        module_name = self._module_name
        datadir = self._get_datadir()
        if datadir:
            files = os.listdir(datadir)
            with open(os.path.join(datadir, module_name), 'wb') as file:
                return pickle.dump(datas, file)

    def save(self):
        """ Save the datas to a file """
        self._save_datas(self._datas)

    @property
    def datas(self):
        return self._datas
