#!/usr/bon/python3
# -*- coding: utf-8 -*-

class Singleton(object):
    _instance = None
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    @classmethod
    def instance(cls):
        assert cls._instance != None
        return cls._instance

    @classmethod
    def has_instance(cls):
        return cls._instance != None


class DictSingleton(dict, Singleton):
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = dict.__new__(cls, *args, **kwargs)
        return cls._instance
