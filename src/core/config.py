#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import logging
import logging.config
from .utils import DictSingleton

class ConfigError(Exception):
    pass


class ConfigHook(object):
    """ Interface for a config hook """
    def on_reload(self, old_config, new_config):
        pass


class Config(DictSingleton):
    def __new__(cls, *args, **kwargs):
        return DictSingleton.__new__(cls, *args, **kwargs)

    def __init__(self, filename):
        logging.config.fileConfig("logging.conf")
        self._module_logger = logging.getLogger("modules")
        self._core_logger = logging.getLogger("core")
        self._filename = filename
        self._parse(filename)
        self._hooks = []

    def __contains__(self, attr):
        return attr in self._contents

    def __getitem__(self, attr):
        try:
            return self._contents[attr]
        except KeyError:
            raise ConfigError("Malformed config file %s: missing '%s'." %
                    (self._filename, attr))

    def add_hook(self, hook):
        """
        Add a hook whose methods will be called on load, reload, quit, etc
        It must derive from the ConfigHook interface
        """
        assert isinstance(hook, ConfigHook)
        self._hooks.append(hook)

    def _parse(self, filename):
        with open(filename) as file:
            contents = json.load(file)
        #if "servers" not in contents or "modules" not in contents:
        #    raise ConfigError("Malformed config file %s: missing 'servers' or"
        #                      " 'modules'" % filename)

        self._contents = contents
        self.servers = self["servers"]
        self.modules = self["modules"]

    def save(self):
        cfg = { "servers": self.servers, "modules": self.modules }
        with open(self._filename, "w") as file:
            json.dump(cfg, file, sort_keys=True, indent=4, encoding="utf-8")

    def reload(self):
        old_contents = dict(self._contents)
        del self._contents
        try:
            self._parse(self._filename)
        except Exception as err:
            logging.error("Unable to reload config: %s" % err)
            self._contents = old_contents
        else:
            new_contents = self._contents
            for hook in self._hooks:
                hook.on_reload(old_contents, new_contents)

    @property
    def module_logger(self):
        return self._module_logger

    @property
    def core_logger(self):
        return self._core_logger

def debug(message):
    Config.instance().core_logger.debug(message)

def info(message):
    Config.instance().core_logger.info(message)

def error(message):
    Config.instance().core_logger.error(message)
