#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from ..core.cron import TimedEvent
from datetime import datetime

class AutoReplyExample(BaseModule):
    # Dérive de BaseModule pour être reconnu lors de l'importation automatique 
    # des modules
    def __init__(self, bot):
        # Le constructeur appelle le constructeur parent, qui initialise
        # l'attribut _bot ('_' signifie symboliquement "protected")
        BaseModule.__init__(self, bot, with_datas=True)

    def on_PRIVMSG(self, msg):
        # lorsque le bot reçoit une commande irc PRIVMSG, il appellera cette
        # méthode (parmis d'autres) avec le message en argument
        bot = self._bot
        # l'attribut params contient les paramètres du message
        # (voir core/protocol.py pour une liste des commandes et leurs
        #  paramètres)
        dest = msg.params[0]
        message = msg.params[1]
        if dest in bot.chans:
            # Si le message a été envoyé sur un chan, on répond dessus
            if message.startswith(bot.nick):
                # si le message est addressé au bot, on peut renvoyer un PRIVMSG
                bot.privmsg_cmd(dest, "%s: kikoo" % msg.user.nick)
        elif dest == bot.nick:
            # sinon, on répond en privé
            bot.privmsg_cmd(msg.user.nick, "kikoo")

        # Note: je compte simplifier les choses en ajoutant des hooks
        #       artificiels du genre on_PRIVMSG_CHAN et on_PRIVMSG_QUERY.
        #       Ainsi que des messages contenant des trucs plus clairs que
        #       "params[0]", qui est trop bas niveau.
        #       Il suffit de mettre un module de traitement des messages
        #       intermédiaire.
        #       D'ailleurs c'est fait (mais pas avec un module) : 

    def on_PRIVMSG_CHAN(self, msg):
        # Affiche "#chan: user> message" dans les logs
        self.debug("%s: %s> %s" % (msg.dest, msg.exp, msg.msg))

    def on_PRIVMSG_QUERY(self, msg):
        # Affiche ">>> user: message" dans les logs
        self.debug(">>> %s: %s" % (msg.exp, msg.msg))

    #@BaseModule.timed_function(TimedEvent(seconds=42))
    #@BaseModule.savedatas
    #def say_time(self):
    #    self.datas["times"] = self.datas.get("times", 0) + 1
    #    self._bot.privmsg_cmd("#firebot",
    #            datetime.now().strftime("IL EST %H:%M:%S ET TOUT VA BIEN! ")
    #            + ("(%d)" % self.datas["times"]))

# Après avoir écrit un tel fichier, il suffit de l'ajouter à la liste des
# modules dans le fichier de config, et c'est tout.
