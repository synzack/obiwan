#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from ..core.user import User

class ChanUpdater(BaseModule):
    def __init__(self, bot):
        BaseModule.__init__(self, bot)

    def _get_chan(self, chan_name):
        """ Get the appropriate Chan instance from the bot """
        return self._bot.chans[chan_name]

    def on_RPL_NAMREPLY(self, msg):
        """ On /name command (or /join) """
        chan = msg.params[2]
        names = msg.params[3].split(" ")
        names = [n.lstrip("@%+&~") for n in names]
        self.bot.whois_cmd(names)
        self._get_chan(chan).users.update(User(u) for u in names)

