#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from subprocess import check_output, CalledProcessError
from random import randint

class Pat(BaseModule):
    def __init__(self, bot):
        BaseModule.__init__(self, bot)

    @BaseModule.command_hook("pat", "[personne]: distribution gratuite de chaleur droïdesque")
    def on_PRIVMSG_CHAN(self, msg):
        nick = msg.msg if msg.msg else msg.exp
        self._bot.privmsg_cmd(msg.dest, "%s: %s" % (nick, " ".join(["*pat*"] * randint(2, 5))))



