#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from subprocess import call, CalledProcessError
import re

QUOTE_RE = re.compile(r"<(?P<nick>[^>]*)>\s*(?P<msg>[^<]*)", re.X | re.I)
QUOTES_FILE = "/home/fire/public_html/wiki/data/pages/fire/quote.txt"

class Quote(BaseModule):
    def __init__(self, bot):
        BaseModule.__init__(self, bot)

    def add_quote(self, quote):
        with open(QUOTES_FILE, "a") as file:
            file.write("\n\n" + quote)

    @BaseModule.command_hook("quote", "<quote>: Ajoute une citation à la base de données (exemple: \"!quote <crét1> message stupide <i2> réponse stupide mais cocasse\")")
    def on_PRIVMSG_CHAN(self, msg):
        """
        Searches for links in the message, add them to the db.
        """
        matches = QUOTE_RE.findall(msg.msg)
        if not matches:
            return
        elif len(matches) == 1:
            self.add_quote((lambda a: "\"%s\" - %s" % (a[1].rstrip().lstrip(), a[0]))(matches[0]))
        else:
            self.add_quote("\\\\\n".join(("<%s> %s" % v) for v in matches))

