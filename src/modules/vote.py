#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from ..core.cron import TimedEvent
from datetime import datetime


class Vote(BaseModule):
    def __init__(self, bot):
        BaseModule.__init__(self, bot, with_datas=True)

    @BaseModule.command_hook("vote", "<value1,value2,...> <title>: soumettre un vote")
    @BaseModule.savedatas
    def on_PRIVMSG_CHAN_submit(self, msg):
        if not " " in msg.msg:
            self.bot.send(msg.build_reply("Pas assez d'arguments."))
            return
        str_values, title = msg.msg.split(" ", 1)
        values = str_values.split(",")
        self.datas["values"] = values
        self.datas["title"] = title
        self.datas["votes"] = dict()
        self.bot.privmsg_cmd(msg.dest, "[VOTE] %s" % title)
        for i, v in enumerate(values):
            self.bot.privmsg_cmd(msg.dest, "%d. %s" % (i, v))

    @BaseModule.command_hook("v", "[id]: afficher le vote en cours ou voter pour un choix")
    @BaseModule.savedatas
    def on_PRIVMSG_CHAN_vote(self, msg):
        def error(response):
            self.bot.send(msg.build_reply(response))
        datas = self.datas
        if not "values" in datas or not "title" in datas:
            error("Pas de vote en cours")
            return
        if not msg.msg:
            error("[Vote en cours]: %s" % datas["title"])
            error("Choix: %s" %
                ", ".join(("%d:%s" % (i, v)) for i, v in enumerate(datas["values"])))
            return
        if msg.exp in datas["votes"]:
            error("vous avez déjà voté, coquinou.")
            return
        try:
            value = int(msg.msg)
        except:
            error("ceci n'est pas un nombre.")
            return
        if value < 0 or value > len(datas["values"]):
            error("valeur erronée")
            return
        datas["votes"][msg.exp] = value

    @BaseModule.command_hook("poil", ": affiche les résultats du vote en cours")
    def on_PRIVMSG_CHAN_poll(self, msg):
        datas = self.datas
        if not "votes" in datas or not datas["votes"]:
            self.bot.send(msg.build_reply("pas de résultats"))
            return

        results = {}
        for val in datas["votes"].values():
            results[val] = results.get(val, 0) + 1
        sorted_results = sorted(results.items(), key=(lambda x: x[1]))
        for result in sorted_results:
            self.bot.privmsg_cmd(msg.dest, "%d%%: %s" % 
                    (result[1] / len(datas["votes"]) * 100,
                     datas["values"][result[0]]))

