#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule

class Helper(BaseModule):
    def __init__(self, bot):
        BaseModule.__init__(self, bot)

    def format_help(self, subject, help_dict):
        out = ""
        if subject:
            if subject in help_dict:
                out = "!%s %s" % (subject, help_dict[subject])
            else:
                out = "pas d'aide pour `%s'" % subject
        else:
            out = "commandes disponibles: %s" % ", ".join(help_dict.keys())
        return out

    @BaseModule.command_hook("help", "[topic]: aide concernant une commande")
    def on_PRIVMSG_CHAN(self, msg):
        commands = dict(self._bot.module_loader.help())
        help_message = self.format_help(msg.msg, commands)
        self._bot.send(msg.build_reply(help_message))

    @BaseModule.command_hook("help", "[topic]: aide concernant une commande")
    def on_PRIVMSG_QUERY(self, msg):
        commands = dict(self._bot.module_loader.help(True))
        help_message = self.format_help(msg.msg, commands)
        self._bot.send(msg.build_reply(help_message))

