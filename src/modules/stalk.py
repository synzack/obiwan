#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from subprocess import check_output, CalledProcessError

class AutoReplyExample(BaseModule):
    def __init__(self, bot):
        BaseModule.__init__(self, bot)

    @BaseModule.command_hook("stalk", "[personne]: donne des indications sur la position approximative"
                           " d'une personne dans le monde.")
    def on_PRIVMSG_CHAN(self, msg):
        if not msg.msg:
            user = "gens"
        else:
            user = msg.msg
        try:
            # TODO: stalk in python3
            ret = check_output(["python", "/home/fire/irc_bot/stalk.py",
                user]).decode("utf-8", "ignore").strip()
        except CalledProcessError:
            ret = "Fail"
        self._bot.send(msg.build_reply(ret))


