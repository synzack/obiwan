#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
#from subprocess import check_output, CalledProcessError

import os

class TheGame(BaseModule):
  file_path = "/tmp/thegame"
  chan = "#firebot"
  mystery_string = ""
  progress = ""
  nick = ""

  def __init__(self, bot):
    BaseModule.__init__(self, bot)

  def init_progress(self):
    for char in self.mystery_string:
      self.progress += ('*' if char != ' ' else ' ')

  def remove_enclosing_quotes(self, str):
    if ((str[0] == '"') or (str[0] == "'")):
      str = str[1:]
    if ((str[-1] == '"') or (str[-1] == "'")):
      str = str[:-1]
    return str

  def update_progress(self, str):
    i = 0
    end = 1

    for char in self.mystery_string:
      if ((i < len(str)) and (char == str[i]) and (self.progress[i] == '*')):
        self.progress = self.progress[:i] + str[i] + self.progress[i+1:]
      if (self.progress[i] == '*'):
        end = 0
      i += 1
    return end

  def load(self):
    with open(self.file_path, 'r') as file:
      self.mystery_string = file.readline()
      if (self.mystery_string[-1] == '\n'):
        self.mystery_string = self.mystery_string[:-1]
      self.progress = file.readline()
      if (self.progress[-1] == '\n'):
        self.progress = self.progress[:-1]

  def save(self):
    with open(self.file_path, 'w') as file:
      file.write("%s\n%s\n" % (self.mystery_string, self.progress))


  @BaseModule.command_hook("thegame", "<answer>")
  def on_PRIVMSG_CHAN(self, msg):
    try:
      self.load()
    except IOError:
      self._bot.privmsg_cmd(msg.dest, "It appears there is no running game")
      return
    if (self.nick == msg.exp):
      self._bot.privmsg_cmd(msg.dest, "You big cheater are not suppose to play this game")
      return
    end = self.update_progress(self.remove_enclosing_quotes(msg.msg))
    if (end == 1):
      self._bot.privmsg_cmd(self.chan, "All glory to %s" % (msg.exp))
      os.remove(self.file_path)
    else:
      self.save()
      self._bot.privmsg_cmd(self.chan, self.progress)

  @BaseModule.command_hook("thegame", "<phrase to discover>")
  def on_PRIVMSG_QUERY(self, msg):
    try:
      self.load()
    except IOError:
      self.mystery_string = self.remove_enclosing_quotes(msg.msg)
      self.init_progress()
      self.nick = msg.exp
      self.save()
      self._bot.privmsg_cmd(self.chan, "I would like to play a game")
      self._bot.privmsg_cmd(self.chan, self.progress)
      self._bot.privmsg_cmd(msg.exp, "The game has started")
      return
    self._bot.privmsg_cmd(msg.exp, "You should play with the others")
