
#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from subprocess import check_output, CalledProcessError

class Say(BaseModule):
    def __init__(self, bot):
        BaseModule.__init__(self, bot)

    @BaseModule.command_hook("say", "<personne> <message>: message en query ou sur un canal.")
    def on_PRIVMSG_QUERY(self, msg):
        out = (msg.exp, "!say <personne> <message>") # todo: return help 3rd opt param
        if ' ' in msg.msg:
            dest, out_msg = msg.msg.split(" ", 1)
            if out_msg:
                out = (dest, out_msg)
        #print("/".join(out))
        #return
        self._bot.privmsg_cmd(*out)



