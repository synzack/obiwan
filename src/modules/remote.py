#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from ..core.cron import TimedEvent
from ..core.config import Config
import os

class RemoteWatcher(BaseModule):
    """
    Use a fifo file to read user commands
    
    Current commands are:
        say #chan A message
        reload
    """
    def __init__(self, bot):
        cfg = Config.instance()["global"]
        self.file_name = file_name = os.path.join(cfg["data_directory"], cfg["remote_file"])
        self._create_fifo(file_name)
        self.locked = False
        BaseModule.__init__(self, bot)

    def _create_fifo(self, file_name):
        if os.path.exists(file_name):
            os.remove(file_name)
        try:
            os.mkfifo(file_name)
        except OSError as err:
            self.error("while trying to create fifo file '%s': %s" % (file_name, err))

    def __del__(self):
        os.remove(self.file_name)

    def __locked(fun):
        def new_fun(self, *args, **kwargs):
            if self.locked:
                return
            self.locked = True
            ret = fun(self, *args, **kwargs)
            self.locked = False
            return ret
        return new_fun

    @BaseModule.timed_function(TimedEvent(seconds=range(0, 60, 2)))
    @__locked
    def check_commands(self):
        """ Check the fifo file for added commands """
        try:
            fd = os.open(self.file_name, os.O_NONBLOCK|os.O_RDONLY)
            file = os.fdopen(fd)
        except OSError:
            # try to recover (curious bug with random fifo deletion)
            self._create_fifo(self.file_name)
            return
        try:
            lines = file.readlines()
            if not lines:
                return
            for line in lines:
                self.handle_command(line.rstrip())
        except OSError as err:
            self.error("while reading file %s: %s" % (self.file_name, err))
        finally:
            file.close()

    def handle_command(self, line):
        self.info("Remote command received: \"%s\"" % line)
        if ' ' in line:
            cmd, args = line.split(" ", 1)
        else:
            cmd, args = line, ""
        cmd = cmd.lower()
        if cmd == "say" and ' ' in args:
            dest, msg = args.split(" ", 1)
            if dest and msg:
                self.bot.privmsg_cmd(dest, msg)
        elif cmd == "reload":
            assert Config.has_instance()
            Config.instance().reload()

