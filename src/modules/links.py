#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ..core.modules import BaseModule
from subprocess import call, CalledProcessError
import re

HTTP_RE = re.compile(r"""(?xi)
\b
(                       # Capture 1: entire matched URL
  (?:
    https?://               # http or https protocol
    |                       #   or
    www\d{0,3}[.]           # "www.", "www1.", "www2." … "www999."
    |                           #   or
    [a-z0-9.\-]+[.][a-z]{2,4}/  # looks like domain name followed by a slash
  )
  (?:                       # One or more:
    [^\s()<>]+                  # Run of non-space, non-()<>
    |                           #   or
    \(([^\s()<>]+|(\([^\s()<>]+\)))*\)  # balanced parens, up to 2 levels
  )+
  (?:                       # End with:
    \(([^\s()<>]+|(\([^\s()<>]+\)))*\)  # balanced parens, up to 2 levels
    |                               #   or
    [^\s`!()\[\]{};:'".,<>?«»“”‘’]        # not a space or one of these punct chars
  )
)""", re.X | re.I)


class Links(BaseModule):
    def __init__(self, bot):
        BaseModule.__init__(self, bot)

    def add_link(self, link, tags, comment=""):
        script = "/home/chicken/scripts/addlink.sh"
        call(["bash", script, link, comment, tags])

    def get_tags(self, message):
        strip = lambda s: s.rstrip().lstrip()
        if message.find("~") >= 0:
            tags = message.split("~")[-1]
            out_tags = []
            for tag in tags.split():
                if ',' in tag:
                    out_tags.extend(strip(t) for t in tag.split(",") if strip(t))
                elif tag:
                    out_tags.append(strip(tag))
            return " ".join(out_tags)
        else:
            return ""

    def on_PRIVMSG_CHAN(self, msg):
        """
        Searches for links in the message, add them to the db.
        """
        match = HTTP_RE.search(msg.msg)
        if match:
            url = match.group(0)
            tags = self.get_tags(msg.msg)
            self.add_link(url, tags, msg.exp)


