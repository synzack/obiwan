#!/bin/bash

cd $(dirname $0)
. setenv.sh

DEST="$1"
shift
MSG="$@"

echo say "$DEST" "$MSG" >> data/remote.fifo
echo say "$DEST" "$MSG" >> data/remote.logs
