#!/usr/bin/python3

from .core.bot import Bot
from .core.config import Config
from time import sleep
from sys import argv, exit

if __name__ == "__main__":
    if not argv[1:]:
        print("Usage: %s <config file>" % argv[0])
        exit(1)
    cfg = Config(argv[1])
    bot = Bot()
    bot.start()
    try:
        while True:
            sleep(1)
    except KeyboardInterrupt:
        pass
    finally:
        bot.close()
        del bot
